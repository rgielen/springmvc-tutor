package net.rgielen.spring.mvc.business.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import net.rgielen.spring.mvc.business.SpringBusinessConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={SpringBusinessConfig.class})
public class FooServiceTest {

	@Autowired private FooService fooService;
	
	@Test
	public void fooServiceGetsInjected() {
		assertNotNull(fooService);
	}

}
