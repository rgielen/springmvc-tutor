package net.rgielen.spring.mvc;

import net.rgielen.spring.mvc.business.SpringBusinessConfig;
import net.rgielen.spring.mvc.web.SpringMvcConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.annotation.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited

@ContextConfiguration(classes = {SpringBusinessConfig.class, SpringMvcConfig.class})
@WebAppConfiguration
public @interface WebTest {
}
