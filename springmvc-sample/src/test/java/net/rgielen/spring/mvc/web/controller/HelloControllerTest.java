package net.rgielen.spring.mvc.web.controller;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class HelloControllerTest {

	@Test
	public void test() {
		assertEquals("rene", new HelloController()
				.sayHello("rene", 19)
				.getModel()
				.get("nameToRender"));
	}

	@Test @Ignore("Demonstrate that parameter default values don't apply to unit tests")
	public void testSayNothing() {
		assertEquals("Spring Course", new HelloController()
				.sayHello(null, 20)
				.getModel()
				.get("nameToRender"));
	}

}
