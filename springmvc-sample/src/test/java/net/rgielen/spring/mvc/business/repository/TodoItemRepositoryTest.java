package net.rgielen.spring.mvc.business.repository;

import net.rgielen.spring.mvc.business.SpringBusinessConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBusinessConfig.class)
@Transactional
public class TodoItemRepositoryTest {

    @Autowired
    TodoItemRepository todoItemRepository;

    @Test
    public void testRepoWorks() throws Exception {
        assertNotNull(todoItemRepository);
    }

}