<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spring MVC Tutor</title>
</head>
<body>
<h1>Spring MVC Tutor</h1>

<ul>
    <li><a href="${s:mvcUrl('TIC#list').build()}">Show Todos</a></li>
</ul>

<form action="<s:url value="/logout"/>" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<button type="submit">Logout</button>
</form>

</body>
</html>