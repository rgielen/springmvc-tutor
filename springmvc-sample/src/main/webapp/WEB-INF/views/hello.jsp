<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href='<c:url value="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/>'/>
    <title><s:message code="hello.title"/> Spring Training Course</title>
</head>
<body>

<h1><s:message code="hello.title"/> <c:out value="${requestScope.nameToRender}"/>,
    soon to be at age <c:out value="${requestScope.ageToRender}"/></h1>

</body>
</html>