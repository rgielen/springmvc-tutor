<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href='<c:url value="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/>'/>
        <title>Please Login</title>
</head>
<body>
<c:url value="/login" var="loginUrl"/>
<form:form name="f" action="${loginUrl}" method="post">
    <fieldset>
        <legend>Please Login</legend>
        <c:if test="${param.error != null}">
            <div class="alert alert-error">
                Invalid username and password.
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <div class="alert alert-success">
                You have been logged out.
            </div>
        </c:if>
        <label for="username">Username</label>
        <input type="text" id="username" name="username" value="${username}"/>
        <label for="password">Password</label>
        <input type="password" id="password" name="password"/>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <div class="form-actions">
            <button type="submit" class="btn">Log in</button>
        </div>
    </fieldset>
</form:form>
</body>
</html>
