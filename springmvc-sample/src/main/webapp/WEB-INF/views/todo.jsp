<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" href='<c:url value="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/>'/>
    <title>Todos</title>
</head>
<body>
<div class="container">

    <h1>Edit Todo</h1>

    <f:form method="post" modelAttribute="item">
        <div class="form-group">
            <label for="topic" class="control-label">Topic</label>
            <f:input path="topic" cssClass="form-control"/>
            <f:errors path="topic"/>
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <f:input path="description" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<s:url value="/todos"/>" class="btn btn-primary" role="button">Back to List</a>
        </div>
    </f:form>

</div>
</body>
</html>
