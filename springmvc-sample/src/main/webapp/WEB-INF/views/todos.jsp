<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <link rel="stylesheet" href='<c:url value="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"/>'/>
    <title>Todos</title>
</head>
<body>
<div class="container">

    <h1>Todos</h1>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${todoItems}" var="item">
            <tr>
                <td><a href="<s:url value="/todos/${item.id}"/>"><c:out value="${item.topic}"/></a></td>
                <td><c:out value="${item.description}"/></td>
                <td></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <a href="<s:url value="/todos/new"/>" class="btn btn-primary" role="button">Create Todo</a>

</div>
</body>
</html>
