package net.rgielen.spring.mvc.web.controller;

import net.rgielen.spring.mvc.business.entity.TodoItem;
import net.rgielen.spring.mvc.business.repository.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

import static org.springframework.http.MediaType.*;
import static org.springframework.http.MediaType.APPLICATION_XHTML_XML_VALUE;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Controller
@RequestMapping("/todos")
public class TodoItemController {

    private final TodoItemRepository repository;

    @Autowired
    public TodoItemController(TodoItemRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public ModelAndView list() {
        return new ModelAndView("todos")
                .addObject("todoItems", repository.findAll());
    }

    @GetMapping("/new")
    public ModelAndView editNew() {
        return new ModelAndView("todo")
                .addObject("item", new TodoItem());
    }

    @PostMapping("/new")
    public String create(@Valid @ModelAttribute("item") TodoItem item, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "todo";
        }
        TodoItem savedItem = repository.save(item);
        return "redirect:/todos/" + savedItem.getId();
    }

    @GetMapping(value = "/{id}", produces = {TEXT_HTML_VALUE, APPLICATION_XHTML_XML_VALUE})
    public ModelAndView view(@PathVariable("id") Long id) {
        return new ModelAndView("todo")
                .addObject("item", repository.findOne(id));
    }

    @PostMapping("/{id}")
    public RedirectView update(@PathVariable("id") Long id, @ModelAttribute("item") TodoItem item) {
        item.setId(id);
        TodoItem savedItem = repository.save(item);
        return new RedirectView(savedItem.getId().toString(), true);
    }

    @ResponseBody
    @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public TodoItem viewAsJson(@PathVariable("id") Long id) {
        return repository.findOne(id);
    }

}
