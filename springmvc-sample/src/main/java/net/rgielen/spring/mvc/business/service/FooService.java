package net.rgielen.spring.mvc.business.service;

import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
public class FooService {
}
