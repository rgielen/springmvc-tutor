/**
 * Package to place Spring Data repositories.
 */
package net.rgielen.spring.mvc.business.repository;