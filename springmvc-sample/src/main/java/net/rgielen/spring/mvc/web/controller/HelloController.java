package net.rgielen.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView sayHello(
            @RequestParam(required = false, defaultValue = "Spring Course") String username,
            @RequestParam(name = "age", required = false, defaultValue = "0") Integer ageValue
    ) {
        ModelAndView modelAndView = new ModelAndView("hello");
        modelAndView.addObject("nameToRender", username);
        modelAndView.addObject("ageToRender", ageValue.intValue() + 1);
        return modelAndView;
    }

}
