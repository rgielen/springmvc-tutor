package net.rgielen.spring.mvc.web;

import org.apache.logging.log4j.core.config.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Order(2)
public class SecurityInititializer extends AbstractSecurityWebApplicationInitializer {
}
