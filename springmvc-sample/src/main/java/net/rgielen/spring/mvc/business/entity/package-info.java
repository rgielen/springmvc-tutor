/**
 * Package to place JPA entities.
 * Will be referenced in {@link net.rgielen.spring.mvc.business.SpringBusinessConfig#entityManagerFactory()} for automatic scanning.
 */
package net.rgielen.spring.mvc.business.entity;