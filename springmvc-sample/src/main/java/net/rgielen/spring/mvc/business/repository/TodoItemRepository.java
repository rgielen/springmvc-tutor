package net.rgielen.spring.mvc.business.repository;

import net.rgielen.spring.mvc.business.entity.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface TodoItemRepository extends JpaRepository<TodoItem,Long> {

    List<TodoItem> findByTopic(String topic);

}
